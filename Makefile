CC = g++

all: mkoutputdir bin/program
bin/TrieNode.o: src/TrieNode.h src/TrieNode.cpp	
	$(CC) -c src/TrieNode.cpp -o bin/TrieNode.o
bin/Trie.o: src/Trie.h src/Trie.cpp
	$(CC) -c src/Trie.cpp -o bin/Trie.o
bin/Phonems.o: src/Phonems.h src/Phonems.cpp
	$(CC) -c src/Phonems.cpp -o bin/Phonems.o
bin/Word.o: src/Word.h src/Word.cpp
	$(CC) -c src/Word.cpp -o bin/Word.o
bin/main.o: src/main.cpp src/Trie.h src/TrieNode.h src/Word.h src/Phonems.h
	$(CC) -c  src/main.cpp -o bin/main.o
bin/program:  bin/TrieNode.o bin/Trie.o  bin/Phonems.o bin/Word.o bin/main.o 
	$(CC) bin/main.o  bin/Trie.o bin/TrieNode.o bin/Phonems.o bin/Word.o  -o bin/program
mkoutputdir:
	mkdir -p bin
clean:
	rm -r -f bin
run:
	./bin/program
