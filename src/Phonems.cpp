#include"Phonems.h"

map<string, int> Phonems::phonems_dictionary;
map<int, string> Phonems::inv_phonems_dictionary;

bool Phonems::insert_phonem_into_dictionary (const string & s) 
{
		if ( s. empty()) return false;
		int slen = s.length();			
		int lqpos = s.find('"');				
		if (lqpos == s.npos) return false;
		int rqpos = s.find('"', lqpos + 1);					
		if (rqpos == s.npos) return false;	 
		int slashpos = s.rfind('/');
		if (slashpos == s.npos) return false;
								 
		string ph = s.substr(lqpos + 1, rqpos - lqpos - 1);
		stringstream sstream( s.substr(slashpos + 1, slen - slashpos));
		int ph_id;
		sstream >> ph_id;	
		phonems_dictionary[ph] = ph_id;
		inv_phonems_dictionary[ph_id] = ph;
		//cout << ph << " " << ph_id << endl;
		return true;		
}

void Phonems::load_dictionary(const string & input_filename)
{
	cout << "Creating dictionary from file " << input_filename << endl;		
	ifstream is(input_filename.c_str());
	string s;
	while (getline(is, s)) insert_phonem_into_dictionary(s);			
}
	

