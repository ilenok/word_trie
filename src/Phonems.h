#ifndef PHONEMS_H
#define PHONEMS_H

#include<iostream>
#include<sstream>
#include<fstream>
#include<string>
#include<map>


using std:: string;
using std::stringstream;
using std::ifstream;
using std::map;
using std::cout;
using std::endl;

class Phonems{
	public:
	static map<string, int> phonems_dictionary;
	static map<int, string> inv_phonems_dictionary;
	static void load_dictionary(const string & input_filename);
	private:
	static bool insert_phonem_into_dictionary (const string & s);	
};

#endif


