#include"Trie.h"
#include"Word.h"

#include<set>

using std::ifstream;
using std::map;
using std::pair;
using std::make_pair;
using std:: set;
using std::endl;


Trie::Trie()
{
	root_node = new TrieNode(0, NODE_TYPE_BEGIN);			
}

Trie::~Trie()
{}

void Trie::print_vertices(ostream & myostream)
{
	for (node_vec::iterator i = word_nodes.begin(); i != word_nodes.end(); ++ i)
		myostream << (*i) -> id << " " << (*i) -> label << endl;
	for (node_vec::iterator i = phonem_nodes.begin(); i != phonem_nodes.end(); ++ i)
		myostream << (*i) -> id << " " << (*i) -> label << endl;
}

void Trie::print_edges(ostream & myostream)
{
	for (node_list :: iterator j = (root_node)-> children.begin(); j != (root_node)->children.end(); ++ j)
	{
		myostream<< (root_node)->id << " "; 
		myostream << (*j)-> id << " " << (*j)->label << "\n";
	}
	for (node_vec::iterator i = phonem_nodes.begin(); i != phonem_nodes.end(); ++ i)
		for (node_list :: iterator j = (*i )-> children.begin(); j != (*i)->children.end(); ++ j)
		{
			myostream<< (*i)->id << " "; 
			myostream << (*j)-> id << " " << (*j)->label << "\n";
		}
	for (node_vec::iterator i = word_nodes.begin(); i != word_nodes.end(); ++ i)
		for (node_list :: iterator j = (*i )-> children.begin(); j != (*i)->children.end(); ++ j)
		{
			myostream<< (*i)->id << " "; 
			myostream << (*j)-> id << " " << (*j)->label << "\n";
		}
}

void Trie::load_words(string input_filename)
{	
		ifstream is(input_filename.c_str());
		string s;
		int int_representation[MAX_WORD_LENGTH];
		int len, id;
		while (getline(is, s)) 
		{
			if (Word::read_word(s, int_representation, len, id))
				insert_word(int_representation, len, id);	
		}	
}


void Trie::insert_word(int * word, int len, int word_id)
{
	int j  = 0;	
	TrieNode * cur_node = root_node;
	TrieNode * next_node;	
	while (next_node = cur_node -> find_child(word[j])) 
	//The current prefix is in the trie
	{
		++ j;
		cur_node = next_node;
	}
	
	//The remaining suffix is not in the trie yet
	while (j < len)
	{			
		TrieNode * new_node = new TrieNode(get_nodes_number(), NODE_TYPE_PHONE, word[j]);	
				
		new_node->parent = cur_node;			
		(cur_node->children).push_back(new_node); 
		//inserting new node		
		phonem_nodes.push_back(new_node);
									
		cur_node = new_node;
		++ j;		
	}
	
	//And now we insert the word id
	
	TrieNode * new_node = new TrieNode(get_nodes_number(), NODE_TYPE_WORD, word_id);
	new_node->parent = cur_node;			
	(cur_node->children).push_back(new_node); 
	word_nodes.push_back(new_node);
		
}

void Trie::print_all_words(ostream & myostream)
{
	//dfs
	string word[MAX_WORD_LENGTH];
	int len = -1;
	int id = -1;
	root_node->visit_and_print(word, len, id, myostream);
}


