#ifndef TRIE_H
#define TRIE_H

#include"TrieNode.h"
#include<map>
#include<algorithm>

using std::map;
using std:: pair;

class Trie
{
	public:	
	Trie();	
	void load_words(string input_filename);
	void insert_word(int * w, int len, int word_id);	
	void print_vertices(ostream & myostream);
	void print_edges(ostream & myostream);	
	int get_nodes_number() {return word_nodes.size() + phonem_nodes.size() + 1;}
	int get_phonem_nodes_number() {return phonem_nodes.size();}
	void print_all_words(ostream & myostream);
	/*bool glue_nodes_step(); 
	void glue_nodes();*/
		
	~Trie();
	
	private:
	TrieNode * root_node;
	vector<TrieNode*> word_nodes;
	vector<TrieNode*> phonem_nodes;
	static const int MAX_WORD_LENGTH = 256;	
	
};

#endif

