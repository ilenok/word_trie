#include"TrieNode.h"
#include"Phonems.h"


using std::cout;
using std::endl;

TrieNode:: TrieNode(int init_id, int init_type, int init_label)
{
	id = init_id;
	type = init_type;
	label = init_label;
	cout << "Creating node #" << id << "  of type " << type <<  " Label: ";
	if (type == NODE_TYPE_PHONE)
		cout  << Phonems:: inv_phonems_dictionary[label];
	else 
		cout  << label;
	cout << endl;
}

TrieNode * TrieNode::find_child(int phonem)
{
	for (node_list :: iterator i = children.begin(); i != children.end(); ++ i)
	{
		if ((*i)->type == NODE_TYPE_PHONE) 
			if ((*i) -> label == phonem) return (*i);	
	}
	return NULL;
}

bool TrieNode:: visit_and_print(string * word, int & len, int & word_id, ostream & myostream)
{
	
	if (type == NODE_TYPE_PHONE) 
	{
		word[len] = Phonems::inv_phonems_dictionary[label];
		/*stringstream sstr;
		sstr << id;
		string tmp;
		sstr >> tmp;
		word[len] += " (" + tmp + ")";*/
	
	}
	else if (type == NODE_TYPE_WORD)
	{
		word[len] = "";
		word_id = label;					
	}
	++ len;
	if (children.empty())
	{
		for (int i = 0; i < len; ++ i) 
			myostream << word[i] << " ";
		myostream << " # " << word_id << endl;
	}	
	for (node_list :: iterator i = children.begin(); i != children.end(); ++ i)
			(*i)  -> visit_and_print(word, len, word_id, myostream);		
	
	-- len;
	return true;
}


