#ifndef TRIE_NODE_H
#define TRIE_NODE_H

#include<list>
#include<vector>
#include<string>
#include<iostream>
#include<fstream>

using std::list;
using std::vector;
using std::string;
using std::ostream;

#define NODE_TYPE_BEGIN       1
#define NODE_TYPE_WORD        2
#define NODE_TYPE_PHONE       3

class TrieNode;
typedef list<TrieNode *> node_list;
typedef vector<TrieNode *> node_vec;

class TrieNode
{
	public:
	TrieNode(int init_id, int init_type, int init_label = -1);
	TrieNode * find_child(int phonem);
	bool visit_and_print(string * word, int & len, int & word_id, ostream & myostream);	
	
	int id;
	int type; // in {1, 2,  3}
	int label; //phonem or word id
		
	TrieNode * parent;
	node_list children;
};

#endif

