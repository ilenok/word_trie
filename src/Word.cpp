#include"Word.h"

bool Word::read_word(const string & s, int * int_representation, int & len, int & id)
{
	if ( s. empty()) return false;
	int slen = s.length();		
	int eqpos = s.find('=');
	int sharppos = s.find('#');
	stringstream sstream( s.substr(eqpos + 1, sharppos - eqpos - 1));		
	string ph;
	int i = 0;
	while (sstream >> ph) 
	{
		int_representation[i] = Phonems:: phonems_dictionary[ph];
		 ++ i;
	}
	len = i; 
	sstream.clear();
	sstream.str((s.substr(sharppos + 1, slen - sharppos - 1)));			
	sstream >> id; 		
	cout << "Word: ";
	for (int i = 0; i < len; ++ i) cout << int_representation[i] <<  " "; 
	cout << "id: " << id << endl;
	return true;
}

