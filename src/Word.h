#ifndef WORD_H
#define WORD_H

#include<iostream>
#include<sstream>
#include<fstream>
#include<string>
#include"Phonems.h"

using std::string;


class Word
{
	public:
	static bool read_word(const string & s, int * int_representation, int & len, int & id);
	
};

#endif

