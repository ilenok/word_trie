
#include"Trie.h"
#include"TrieNode.h"
#include"Word.h"
#include"Phonems.h"

using  std:: cout;
using std:: ofstream;

int main ()
{
	Trie trie;		
	Phonems::load_dictionary("Phone.txt");
	
	trie.load_words("dictionary_rs_19935_strict.txt");
	cout << endl << "Edges:" << endl;
	trie.print_edges(cout);	
	cout << "Nodes: " << trie.get_nodes_number();
	cout  << ", including " << trie.get_phonem_nodes_number() << " phonem nodes\n";
	
	ofstream os("out.txt");		
	trie.print_all_words(os);
}
